import os, sys
from getpass import getpass as hidden
class sysc():
    def clear():
        os.system("clear")
sysc.clear()
print("Hanger, type the word!(you won't be able to see what your typing)")
word = hidden("")
print(word)
wordH = []
wrong = []
wordB = []
# Generate hidden chars
for char in word:
    if not char == " ":
        wordH.append("-")
    else:
        wordH.append(" ")
    wordB.append(char)

sysc.clear()
print(str(wordB))
play = True
lives = 5
aused = False
# The actual game
while play:
    sysc.clear()
    print("Lives: " + str(lives))
    print("".join(wordH))
    print("Type 1 letter to guess in the word!")
    if aused == True:
        print("You already tried that! try another.")
        aused = False
    if len(wrong) != 0:
        print("Used characters: " + str(wrong)) 
    guess = input("")
    if guess in wrong:
        aused = True
        continue
    ind = 0
    right = 0
    for char in wordB: 
        if guess == char:
            right += 1
            wordH[ind] = char
        ind += 1
    if right == 0:
        lives -= 1
        wrong.append(guess)
    if lives == 0:
        print("game over")
        exit(0)
    if not "-" in wordH:
        print("Congratulations! you guessed the word! the word was " + word)
        choice = input("play again? ")
        if choice == "yes" or choice == "y":
            os.system("python " + __file__)
        else:
            sysc.clear()
            print("Thanks for playing!")
